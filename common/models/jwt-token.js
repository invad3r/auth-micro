'use strict';
var debug  = require('debug')('oauth:models:jwt-token');
const assert = require('assert');
const jwt = require('jsonwebtoken');

module.exports = function(JwtToken) {
  /**
   * Resolve and validate the jwt token
   * @param {String} token the jwt token
   * @param {String} secret the secret that was used to sign the token
   * @param {Object} Resolved access token object
   */
  JwtToken.resolve = function(token, secret) {
    const promise = new Promise((resolve, reject) => {
      jwt.verify(token, secret, function(err, decoded) {
        if (err)
          return reject(err);
        if (!decoded)
          return reject(new Error('Invalid signature'));

        JwtToken.findById(decoded.jti, function(err, token) {
          if (err || !token)
            return reject(new Error('Cant find token on cache'));

          token.validate().then((token) => {
            if (!token)
              throw new Error('token has expired');

            // let clone  = JSON.parse(JSON.stringify(token));
            if (token.type == 'otp')
              token.destroy();

            return resolve(token);
          }).catch((error) => {
            return reject(new Error('Invalid or missing token'));
          });
        });
      });
    });
    return promise;
  };

  /**
  * Destroy the token with the given id and create a new one
  * with the same data and different id.
  */
  JwtToken.prototype.regenerate = function(id, cb) {
    var token = this;

    JwtToken.findOne({_id: id}, function(err, jwtToken) {
      if (err) return cb(err);

      if (!jwtToken) {
        token.save(function(err, token) {
          if (err) return cb(err);
          return cb(null, token);
        });
      } else {
        jwtToken.destroy(function(err) {
          if (err) return cb(err);

          token.save(function(err, token) {
            if (err) return cb(err);
            return cb(null, token);
          });
        });
      }
    });
  };

  JwtToken.prototype.validate = function() {
    const token = this;
    const promise = new Promise((resolve, reject) => {
      try {
        assert(
            token.created && typeof token.created.getTime === 'function',
          'token.created must be a valid Date'
        );
        assert(token.ttl !== 0, 'token.ttl must be not be 0');
        assert(token.ttl, 'token.ttl must exist');
        assert(token.ttl >= -1, 'token.ttl must be >= -1');

        let now = Date.now();
        let created = token.created.getTime();
        let elapsedSeconds = (now - created) / 1000;
        let secondsToLive = token.ttl;
        const isValid = elapsedSeconds < secondsToLive;
        if (isValid)
          return resolve(token);
        else
          throw new Error('token has expired');
      } catch (e) {
        token.destroy((error) => {
          if (error) return reject(error);
          return resolve(null);
        });
      }
    });
    return promise;
  };

  /**
  * Check for a token whitin the expiration date that match the
  * origin and the fingerprint. The can be only one valid jwt token at the same time
  * for a specific client/device/user.
  */
  JwtToken.prototype.forge = function(secret, cb) {
    debug('forging jwt-token');
    var token = this;
    var signedJWT;
    const User = JwtToken.app.models.user;
    let promise = new Promise((resolve, reject) => {
      JwtToken.findOne({where: {and: [
        {fingerprint: token.fingerprint},
        {userId: token.userId},
      ]}, order: 'created DESC'}).then((oldToken) => {
        debug('validatig old token %s');
        if (!oldToken)
          return token.save();
        else
          return oldToken.validate();
      }).then((validToken) => {
        if (validToken) {
          debug('token validated, reuse old jwt');
          return new Promise((r) => { return r(validToken); });
        } else {
          debug('token expired, creating new jwt');
          return token.save();
        }
      }).then((t) => {
        token = t;
        // fetch user profile to embed identity claims on the JWT
        return User.findById(token.userId);
      }).then((user) => {
        if (!user)
          throw new Error('Invalid user');

        debug('signin json web token');
        const profile = User.parseProfile(user);

        let idClaims = {
          name: profile.name,
          email: profile.email,
          language: profile.language,
        };

        let data = Object.assign(idClaims, {
          iss: token.clientId,
          usr: token.userId,
          jti: token.id,
        });

        if (token.type)
          data.type = token.type;

        if (token.scopes)
          data.scopes = token.scopes;

        if (token.audience)
          data.aud = token.audience;

        if (token.metadata)
          data.metadata = typeof token.metadata == 'object' ?
            JSON.stringify(token.metadata) :
            token.metadata;

        signedJWT = jwt.sign(data, secret, {expiresIn: token.ttl});
        return token.save();
      }).then((token) => {
        return resolve(signedJWT);
      }).catch((error) => {
        debug('Error jwt forgery!', error);
        return reject(error);
      });
    });
    return promise;
  };
};
