'use strict';
var app = require('../../server/server');
var debug  = require('debug')('oauth:models:two-factor-token');

module.exports = function(TwoFactorToken) {
  /**
  * Destroy the token with the given id and create a new one
  * with the same data and different id.
  */
  TwoFactorToken.prototype.regenerate = function(id, cb) {
    var token = this;

    TwoFactorToken.findOne({where: {_id: id}}, function(err, twoFactorToken) {
      if (err) return cb(err);

      if (!twoFactorToken) {
        token.save(function(err, token) {
          if (err) return cb(err);
          return cb(null, token);
        });
      } else {
        twoFactorToken.destroy(function(err) {
          if (err) return cb(err);

          token.save(function(err, token) {
            if (err) return cb(err);
            return cb(null, token);
          });
        });
      }
    });
  };

  /**
  * Check for a token whitin the expiration date that match the
  * origin and the fingerprint. The can be only one valid access token at the same time
  * for a specific client/device/user. The limit can be restricted to one token by
  * client/user, it woud be more secure, but the UX will suffer!!!
  */
  TwoFactorToken.prototype.forge = function(cb) {
    // check for the last valid token that match user, client and fingerprint
    debug('forging two-factor-token');
    var token = this;
    TwoFactorToken.findOne({
      where: {and: [
        {fingerprint: token.fingerprint},
        {userId: token.userId},
      ]},
      order: 'created DESC',
    }, function(err, oldToken) {
      if (err) return cb(err);
      if (oldToken) {
        // validate the old token, if it's not valid it will be destroyed
        oldToken.validate(function(err, isValid) {
          if (isValid) {
            debug('reusing old valid two-factor-token');
            return cb(null, oldToken);
          }
          // if the token it's not valid we create a new one
          token.save(function(err, token) {
            if (err) return cb(err);
            debug('new two-factor-token created');
            return cb(null, token);
          });
        });
      } else {
        // if there's no token then we create one
        token.save(function(err, token) {
          if (err) return cb(err);
          debug('new two-factor-token created');
          return cb(null, token);
        });
      }
    });
  };
};
