'use strict';
var app = require('../../server/server');
var debug  = require('debug')('oauth:models:access-token');

module.exports = function(AccessToken) {
  /**
   * Promise wrapped Resolve method
   * Resolve and validate the access token by id
   * @param {String} id Access token id
   * @return {Object} Resolved access token object
   */
  AccessToken.promiseResolve = function(id) {
    const promise = new Promise((resolve, reject) => {
      this.resolve(id, function(err, token) {
        if (err || !token)
          return reject(new Error('Invalid or missing token'));
        else
          return resolve(token);
      });
    });
    return promise;
  };

  /**
  * Destroy the token with the given id and create a new one
  * with the same data and different id.
  */
  AccessToken.prototype.regenerate = function(id, cb) {
    var token = this;

    AccessToken.findOne({_id: id}, function(err, accessToken) {
      if (err) return cb(err);

      if (!accessToken) {
        token.save(function(err, token) {
          if (err) return cb(err);
          return cb(null, token);
        });
      } else {
        accessToken.destroy(function(err) {
          if (err) return cb(err);

          token.save(function(err, token) {
            if (err) return cb(err);
            return cb(null, token);
          });
        });
      }
    });
  };

  /**
  * Check for a token whitin the expiration date that match the
  * origin and the fingerprint. The can be only one valid access token at the same time
  * for a specific client/device/user. The limit can be restricted to one token by
  * client/user, it woud be more secure, but the UX will suffer!!!
  */
  AccessToken.prototype.forge = function(cb) {
    // check for the last valid token that match user, client and fingerprint
    debug('forging access-token');
    var token = this;
    AccessToken.findOne({
      where: {and: [
        {fingerprint: token.fingerprint},
        {userId: token.userId},
      ]},
      order: 'created DESC',
    }, function(err, oldToken) {
      if (err) return cb(err);
      if (oldToken) {
        // validate the old token, if it's not valid it will be destroyed
        oldToken.validate(function(err, isValid) {
          if (isValid) {
            debug('reusing old valid access-token');
            return cb(null, oldToken);
          }
          // if the token it's not valid we create a new one
          token.save(function(err, token) {
            if (err) return cb(err);
            debug('new access-token created');
            return cb(null, token);
          });
        });
      } else {
        // if there's no token then we create one
        token.save(function(err, token) {
          if (err) return cb(err);
          debug('new access-token created');
          return cb(null, token);
        });
      }
    });
  };
};
