'use strict';
const debug  = require('debug')('oauth:models:refresh-token');
const assert = require('assert');

module.exports = function(RefreshToken) {
  /**
  * Destroy the given token and create a new one
  * with the same data and different id.
  */
  RefreshToken.prototype.regenerate = function() {
    var oldToken = this;
    var newToken = new RefreshToken({
      userId: oldToken.userId,
      clientId: oldToken.clientId,
      fingerprint: oldToken.fingerprint,
      origin: oldToken.origin,
    });

    let promise = new Promise((resolve, reject) => {
      oldToken.destroy().then(() => {
        return newToken.save();
      }).then((token) =>{
        return resolve(token);
      }).catch((error) => {
        return reject(error);
      });
    });
    return promise;
  };

  RefreshToken.prototype.validate = function() {
    const token = this;
    const promise = new Promise((resolve, reject) => {
      try {
        assert(
            token.created && typeof token.created.getTime === 'function',
          'token.created must be a valid Date'
        );
        assert(token.ttl !== 0, 'token.ttl must be not be 0');
        assert(token.ttl, 'token.ttl must exist');
        assert(token.ttl >= -1, 'token.ttl must be >= -1');

        let now = Date.now();
        let created = token.created.getTime();
        let elapsedSeconds = (now - created) / 1000;
        let secondsToLive = token.ttl;
        const isValid = elapsedSeconds < secondsToLive;
        if (isValid)
          return resolve(token);
        else
          throw new Error('token has expired');
      } catch (e) {
        token.destroy((error) => {
          if (error) return reject(error);
          return resolve(null);
        });
      }
    });
    return promise;
  };

  /**
  * Check for a token whitin the expiration date that match the
  * origin and the fingerprint. The can be only one valid access token at the same time
  * for a specific client/device/user. The limit can be restricted to one token by
  * client/user, it woud be more secure, but the UX will suffer!!!
  */

  RefreshToken.prototype.forge = function() {
    debug('forging refresh-token');
    var token = this;
    let promise = new Promise((resolve, reject) => {
      RefreshToken.findOne({where: {and: [
        {fingerprint: token.fingerprint},
        {userId: token.userId},
      ]}, order: 'created DESC'}).then((oldToken) => {
        debug('validatig old token %s');
        if (!oldToken)
          return token.save();
        else
          return oldToken.validate();
      }).then((validToken) => {
        if (validToken) {
          debug('token validated, reuse old refresh-token');
          return new Promise((r) => { return r(validToken); });
        } else {
          debug('token expired, creating new refresh-token');
          return token.save();
        }
      }).then((token) => {
        return resolve(token);
      }).catch((error) => {
        debug('Error on refresh-token forgery!');
        return reject(error);
      });
    });
    return promise;
  };
};
