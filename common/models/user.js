'use strict';
const app = require('../../server/server');
const qs = require('querystring');
const _ = require('lodash');
const debug = require('debug')('loopback:api:auth');
const path = require('path');

module.exports = function(User) {
  /**
   * Get parsed user profile
   * @param {Object} user user model instance
   * @return {Object} user profile
   */
  User.parseProfile = function(user) {
    const profile  = user.profiles && user.profiles[0] ?
      user.profiles[0].profile : null;
    const name = profile ? profile.displayName : user.username;
    const email = profile && profile.emails.length ?
      profile.emails[0].value : user.email;

    return {
      userId: user.id,
      username: user.username || email,
      email: email,
      name: name,
      account: user.email,
      roles: user.roles,
      language: user.language,
      licenseAgreement: user.licenseAgreement,
      no2faRiskAcknowledge: user.no2faRiskAcknowledge,
      created: user.created,
      modified: user.modified,
    };
  };

  /**
   * Find and deserialize a user by id
   * @param {String} id Access token id
   * @return {Object} Resolved user object
   */
  User.deserializeById = function(id) {
    const promise = new Promise((resolve, reject) => {
      this.findById(id, function(err, user) {
        if (err || !user)
          return reject(new Error('Invalid or missing user'));

        user.identities(function(err, identities) {
          user.profiles = identities;
          user.credentials(function(err, accounts) {
            user.accounts = accounts;
            return resolve(user);
          });
        });
      });
    });
    return promise;
  };

  User.hasMatchingRole = function(client) {
    return (_.difference(client.roles, this.roles).length ==
      client.roles.length);
  };

  User.observe('before save', function(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.roles = ['client'];
      ctx.instance.licenseAgreement = true;
      ctx.instance.no2faRiskAcknowledge = false;
      ctx.instance.pin = Math.random().toString().substr(2, 4);
      ctx.instance.firstLogin = true;
      ctx.instance.created =  new Date();
      ctx.instance.realm = ctx.instance.realm ? ctx.instance.realm : 'basic';
      ctx.instance.language = ctx.instance.language ? ctx.instance.language :
        app.get('defaultLanguage');
    }

    if (ctx.instance)
      ctx.instance.modified = new Date();
    if (ctx.date)
      ctx.data.modified = new Date();

    return next();
  });

  User.on('resetPasswordRequest', function(info) {
    const app = User.app;
    const uri = app.get('uri') + '/public/password-reset/form?token=';
    const lang = info.user.language;

    const template = app.loopback.template(path.resolve(__dirname,
      `../../server/views/${lang}/email-template/reset-password.ejs`));

    const html = template({
      uri: uri,
      token: info.accessToken.id,
    });

    var options = _.extend(app.get('email'), {
      to: info.email,
      subject: 'Password reset',
      html: html,
    });

    User.app.models.Email.send(options, function(err, email) {
      if (err) return debug('> error sending password reset email');
      debug('> sending password reset email to:', info.email);
    });
  });

  User.observe('after save', function(ctx, next) {
    if (ctx.isNewInstance) {
      let user = ctx.instance;
      // send verification email after registration
      if (!user.emailVerified) {
        debug('sending email verification');
        const tmplPath = path.resolve(__dirname,
          `../../server/views/${user.language}/email-template`);
        var options = _.extend(app.get('email'), {
          type: 'email',
          to: user.email,
          verifyHref: app.get('verifyPath') +
          '?' + qs.stringify({
            uid: '' + user.id,
            clientId: '' + user.referer,
          }),
          subject: app.loopback.template(`${tmplPath}/verify_subject.ejs`)(),
          template: `${tmplPath}/verify.ejs`,
          user: user,
        });

        user.verify(options, function(err, response) {
          if (err) debug('Error verifying user: ', JSON.stringify(err));
        });
      }
    }
    return next();
  });
};
