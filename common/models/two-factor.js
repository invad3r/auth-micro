'use strict';
const app    = require('../../server/server');
const debug  = require('debug')('loopback:api:two-factor');
const notp   = require('notp');

module.exports = function(TwoFactor) {
  TwoFactor.prototype.verify = function(code, options) {
    options = options || {window: 5};
    const twoFactor = this;
    return notp.totp.verify(code, twoFactor.secret, options);
  };
};
