/* eslint max-len: ["error", 120] */
/* eslint indent: ["error", "tab"] */
'use strict';

require('assert'); // node.js core module
var request = require('request');
var qrcode  = require('qrcode-terminal');

var host  = process.env.HOST || 'http://localhost:3000';
var user = process.env.USER;
var pass = process.env.PASS;
var clientId = process.env.CLIENT;
var session = {};

if (!user) {
	console.log('You must provide a user id on the env variable USER');
	process.exit();
}

if (!pass) {
	console.log('You must provide a password on the env variable PASS');
	process.exit();
}

if (!clientId) {
	console.log('You must provide a client id on the env variable CLIENT');
	process.exit();
}

function ask(question, callback) {
	var stdin = process.stdin;
	var stdout = process.stdout;

	stdin.resume();
	stdout.write(question + ': ');

	stdin.once('data', function(data) {
		data = data.toString().trim();
		callback(data);
	});
}

describe('Try the get, validate, MFA, refresh and destroy token flow for the credentials-owner grant type', function() {
	this.timeout(40000);

	/**
	*	Try the credentials-owner grant
	* Endpoint: /oauth/grant/credentials-owner
	*/
	it('It should grant access and return a signed JWT', function(done) {
		let url = host + '/oauth/grant/credentials-owner';
		request.post({
			url: url,
			auth: {
				user: user,
				pass: pass,
				json: true,
			},
			form: {clientId: clientId},
		}, function(err, res, body) {
			if (res.statusCode == 401) {
 				done(new Error('Invalid user'));
				console.log('    > Invalid user');
				return;
			}

			if (err || res.statusCode != 200) {
				console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
				return done(new Error('Error getting token'));
			} else {
				let token = JSON.parse(res.body).token;
				if (!token) {
					done(new Error('Chek that the user has 2FA disabled for this client'));
					console.log('    > No JWT token, chek that the user has 2FA disabled for this client');
					return;
				} else {
					session.token = token;
					return done();
				}
			}
		});
	});

	/**
	*	Try validating the jwt token against the auth server
	* Endpoint: /oauth/token/verify endpoint
	*/
	it('It should validate the recently obtained token.', function(done) {
		let url = host + '/oauth/token/verify';
		request.post({
			url: url,
			form: {
				clientId: clientId,
				token: session.token,
			},
		}, function(err, res, body) {
			if (res.statusCode == 401) {
				done(new Error('Invalid JWT token'));
				console.log('    > Invalid JWT token');
				return;
			}

			if (err || res.statusCode != 200) {
				console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
				return done(new Error('Error validating token'));
			} else {
				return done();
			}
		});
	});

	/**
	*	Try validating the jwt token against the auth server
	* Endpoint: /oauth/grant/refresh-token
	*/
	it('It should refresh the token using the /oauth/token/refresh endpoint', function(done) {
		let url = host + '/oauth/grant/refresh-token';
		request.post({
			url: url,
			form: {
				clientId: clientId,
				token: session.token,
			},
		}, function(err, res, body) {
			if (res.statusCode == 401) {
				done(new Error('Invalid JWT token'));
				console.log('    > Invalid JWT token');
				return;
			}

			if (err || res.statusCode != 200) {
				console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
				return done(new Error('Error refresing token'));
			} else {
				let token = JSON.parse(res.body).token;
				session.token = token;
				return done();
			}
		});
	});

	/**
	*	Enable the two factor authentication (TOTP) for the current client.
	* Endpoint: /account/two-factor/enable
	*/
	it('It should enable the two factor authentication for the current client', function(done) {
		let url = host + '/account/two-factor/totp/enable?format=uri';
		request.post({
			url: url,
			form: {
				clientId: clientId,
				token: session.token,
			},
		}, function(err, res, body) {
			if (res.statusCode == 401) {
				done(new Error('Invalid JWT token'));
				console.log('    > Invalid JWT token');
				return;
			}

			if (err || res.statusCode != 200) {
				console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
				return done(new Error('Error trying to enable the two factor authentication'));
			} else {
				console.log(); console.log();
				qrcode.generate(body, {small: true});
				console.log(); console.log();
				return done();
			}
		});
	});

	/**
	*	Try the credentials-owner grant with two-factor enabled
	* Endpoint: /oauth/grant/credentials-owner
	*/
	it('It should grant access and return 2FA token', function(done) {
		let url = host + '/oauth/grant/credentials-owner';
		request.post({
			url: url,
			auth: {
				user: user,
				pass: pass,
				json: true,
			},
			form: {clientId: clientId},
		}, function(err, res, body) {
			if (res.statusCode == 401) {
 				done(new Error('Invalid user'));
				console.log('    > Invalid user');
				return;
			}

			if (err || res.statusCode != 200) {
				console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
				return done(new Error('Error getting token'));
			} else {
				let token = JSON.parse(res.body).twoFactorToken;
				session.twoFactorToken = token;
				return done();
			}
		});
	});

	/**
	*	Confirm the two factor authentication
	* Endpoint: /oauth/grant/credentials-owner
	*/
	it('It should validate the 2FA code and return a signed JWT', function(done) {
		ask('    > Enter the authenticator code', function(code) {
			let url = host + '/oauth/two-factor/confirm';
			request.post({
				url: url,
				form: {
					twoFactorToken: session.twoFactorToken,
					twoFactorCode: code,
				},
			}, function(err, res, body) {
				if (res.statusCode == 401) {
 					done(new Error('Invalid 2FA code or token'));
					console.log('    > Invalid 2FA code or token');
					return;
				}

				if (err || res.statusCode != 200) {
					console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
					return done(new Error('Error getting token'));
				} else {
					let token = JSON.parse(res.body).token;
					session.token = token;
					return done();
				}
			});
		});
	});

	/**
	*	Disable the two factor authentication (TOTP) for the current client.
	* Disable: /account/two-factor/enable
	*/
	it('It should disable the two factor authentication for the current client', function(done) {
		let url = host + '/account/two-factor/disable';
		request.post({
			url: url,
			form: {
				clientId: clientId,
				token: session.token,
			},
		}, function(err, res, body) {
			if (res.statusCode == 401) {
				done(new Error('Invalid JWT token'));
				console.log('    > Invalid JWT token');
				return;
			}

			if (err || res.statusCode != 200) {
				console.log('Status response : %s Error: %s, Body: %s', res.statusCode, err, body);
				return done(new Error('Error trying to disable the two factor authentication'));
			} else {
				session.qr = body;
				return done();
			}
		});
	});
});
