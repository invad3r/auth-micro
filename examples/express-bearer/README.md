# express-bearer-integration
This serves a an example for a simple bearer integration using express and the jwt token provided by the authentication service.

## Install & Run
First the client must be registered on the authentication service database. Here is a copy of the document used to test this example.

```
{
    "_id" : ObjectId("5981f34d34af4e37b70fee33"),
    "secret" : "this is a secret",
    "name" : "express-oauth-integration",
    "uniqueSession" : true,
    "domain" : "http://localhost:3001",
    "callbackURL" : "http://localhost:3001/authorized",
    "refreshToken" : true,
    "scopes" : [],
    "created" : ISODate("2017-08-02T04:06:19.148Z")
}```

Once the client data has been saved, proceed to install the dependencies and run the example.

```
npm Install
node index.js
```

In this case you have to get a jwt token on your own, you could do an authorized request to the credentials grant endpoint or just go to the sign in form and manually copy the token from the callback uri.
Once you have it, go to the `/bearer` endpoint attaching the jwt like this `?access_token=xxxxx`. This endpoint is secured using the bearer strategy, if the token is missing or invalid a `401` error will be thrown, otherwise the decoded jwt token will be show. 
