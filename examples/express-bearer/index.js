const _request = require('request')
const express = require('express')
const passport = require('passport')
const BearerStrategy = require('passport-http-bearer').Strategy
const app = express()
const CLIENT_ID = '5981f34d34af4e37b70fee33'

var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cookieParser())

//BEARER INTEGRATION
passport.use(new BearerStrategy(
  function(token, done) {
    var query = `?token=${token}`;
    _request('http://localhost:3000/oauth/token/verify'+query, function(err, response, body){
      let session = JSON.parse(body);
      return done(session.error, session);
    })
  }
));

app.use( passport.initialize() );

app.get('/bearer', passport.authenticate('bearer', {session: false}), function(req, res){
  console.log("User %s authenticated", req.user.userId)
  res.send({
    msg: 'Congrats you are authorized!',
    session: req.user
  })
});

app.listen(3001, function () {
  console.log('Express + passport-bearer integration listening on port 3001')
})
