# web-client
This example shows how to use the implicit grant through a web client application.
Please do not copy this code! Its intended only to show how the authorization flow works.
It also can be useful for debugging the jwt token.

## Install & Run
First the client must be registered on the authentication service database. Here is a copy of the document used to test this example.

```
{
    "_id" : ObjectId("5981f34d34af4e37b70fee55"),
    "secret" : "this is a secret",
    "name" : "simple-web-client",
    "uniqueSession" : true,
    "domain" : "http://localhost:3001",
    "callbackURL" : "http://localhost:3001/authorized.html",
    "refreshToken" : true,
    "scopes" : [],
    "created" : ISODate("2017-08-02T04:06:19.148Z")
}```

Once the client data has been saved, proceed to install the dependencies and run the example.

```
npm install -g http-server
cd examples/web-client/
http-server -p 3001
```
