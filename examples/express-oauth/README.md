# express-oauth2-integration
This serves a an example for a simple oauth2 integration usging express and the implicit grant provided by the authentication service.

## Install & Run
First the client must be registered on the authentication service database. Here is a copy of the document used to test this example, have in mind that if you change the port where the integration will run you also have to change it in the client data.

```
{
    "_id" : ObjectId("5981f34d34af4e37b70fee33"),
    "secret" : "this is a secret",
    "name" : "express-oauth-integration",
    "uniqueSession" : true,
    "domain" : "http://localhost:3001",
    "callbackURL" : "http://localhost:3001/authorized",
    "refreshToken" : true,
    "scopes" : [],
    "created" : ISODate("2017-08-02T04:06:19.148Z")
}```

Once the client data has been saved, proceed to install the dependencies and run the example.

```
npm Install
node index.js
```

Then visit the client root url, it will redirect you to the auth-service implicit grant form. If a successfully login occurs the auth-service will redirect you back to the client success url, in this case `/authorized`, including a jwt token on the query string.
