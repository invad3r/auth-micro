const express = require('express')
const passport = require('passport')
const OAuth2Strategy = require('passport-oauth2')
const app = express()
const CLIENT_ID = '5981f34d34af4e37b70fee33'

app.listen(3001, function () {
  console.log('Express + passport-oauth2 integration listening on port 3001')
})

//OAUTH2 INTEGRATION
passport.use(new OAuth2Strategy({
    authorizationURL: 'http://localhost:3000/oauth/authorize',
    tokenURL: 'http://localhost:3000/oauth/token/verify',
    clientID: CLIENT_ID,
    callbackURL: "http://localhost:3001/auth/example/callback",
    skipUserProfile: true
  },
  function(accessToken, refreshToken, session, profile, cb) {
    return cb(session.error, session);
  }
));

app.use( passport.initialize() );

app.get('/', passport.authenticate('oauth2', {session: false}));

app.get('/authorized', passport.authenticate('oauth2', {session: false}), function(req, res){
  console.log("User %s authenticated", req.user.userId)
  res.send({
    msg: 'Congrats you are authorized!',
    session: req.user
  })
});
