/* eslint indent: ["error", "tab"] */
/* global location, Headers, document, $, window */
'use strict';

function getJsonFromUrl() {
	var result = {};
	location.search.substr(1).split('&').forEach(function(part) {
		var item = part.split('=');
		result[item[0]] = decodeURIComponent(item[1]);
	});
	return result;
}

function getQueryString() {
	const params = getJsonFromUrl();
	var query = [];
	if (params.token)
	  query.push('token=' + params.token);
	if (params.clientId)
	  query.push('clientId=' + params.clientId);
	return '?' + query.join('&');
};

function getHeaders(opts) {
	opts = opts ? opts : {};
	const params = getJsonFromUrl();
	const token = opts.token || params.token;
	const clientId = opts.clientId || params.clientId;
	const headers = opts.headers || {
	  'Content-Type': 'application/json',
	};

	if (token)
	  headers['authorization'] = 'Bearer ' + token;

	if (clientId)
	  headers['client_id'] = clientId;

	return new Headers(headers);
};

// floating labels/placeholders for input fields
$(document).ready(function() {
	$('.animate-label input').each(function() {
		if ($(this).val() != '') {
			$(this).closest('.animate-label').addClass('hascontent');
		}
	});
	$('.animate-label input').bind('blur keyup', function() {
		if ($(this).val() != '') {
			$(this).closest('.animate-label').addClass('hascontent');
			$(this).closest('.animate-label').removeClass('nocontent');
		} else {
			$(this).closest('.animate-label').addClass('nocontent');
			$(this).closest('.animate-label').removeClass('hascontent');
		}
	});
	$('.animate-label select').each(function() {
		if ($(this).val() != '') {
			$(this).closest('.animate-label').addClass('hascontent');
		}
	});
	$('.animate-label select').bind('blur keyup click', function() {
		if ($(this).val() != '') {
			$(this).closest('.animate-label').addClass('hascontent');
			$(this).closest('.animate-label').removeClass('nocontent');
		} else {
			$(this).closest('.animate-label').addClass('nocontent');
			$(this).closest('.animate-label').removeClass('hascontent');
		}
	});
	$('.animate-label textarea').each(function() {
		if ($(this).val() != '') {
			$(this).closest('.animate-label').addClass('hascontent');
		}
	});
	$('.animate-label textarea').bind('blur keyup', function() {
		if ($(this).val() != '') {
			$(this).closest('.animate-label').addClass('hascontent');
			$(this).closest('.animate-label').removeClass('nocontent');
		} else {
			$(this).closest('.animate-label').addClass('nocontent');
			$(this).closest('.animate-label').removeClass('hascontent');
		}
	});

	$('.navbar-toggle').click(function() {
		if ($(window).width() < 768) {
		   $('.overlay').fadeToggle('slow');
		}
	});
	$(function() {
		$('button.profile').click(function() {
			   $('.overlay').fadeToggle('slow');
		});
	});
});
