$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
		$active.prev().addClass('completed');  	
		$(".modal .wizard-inner .progress .progress-bar").width($(".modal .wizard-inner .progress .progress-bar").outerWidth() + 251);	
        nextTab($active);

    });
    $(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
		$active.next().removeClass('completed');
		$(".modal .wizard-inner .progress .progress-bar").width($(".modal .wizard-inner .progress .progress-bar").outerWidth() - 251);
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}



