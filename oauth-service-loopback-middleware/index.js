'use strict';
var request = require('request');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var debug = require('debug')('oauth:middleware');

module.exports = class OauthServiceMiddleware {

  constructor(options) {
    options = typeof options == 'string' ? {host: options} : options;

    if (!options)
      throw new Error('No host specefied for oauth service middleware.');

    this.key = options.key;
    this.secret = options.secret;
    this.host = options.host;
    this.verifyUri = options.verifyUri || (this.host + '/oauth/token/verify');
    this.profileUri = options.profileUri || (this.host + '/account/profile');
    this.attachJWT = options.attachJWT;
  }

  get authenticate() {
    return this.authenticate;
  }

  authenticate(req, res, next) {
    const verifyUri = this.verifyUri;
    const profileUri = this.profileUri;
    const attachJWT = this.attachJWT;
    const apiKey = this.key;
    const apiSecret = this.secret;

    // middleware function
    return function(req, res, next) {
      const app = req.app;
      const User = app.models.User;
      const AccessToken = app.models.AccessToken;
      // const AccessToken = app.models.AccessToken.extend('accessToken', {
      //  jti: {type: String, required: true},
      // });

      // Verify token function
      const verifyToken = function(opts) {
        let promise = new Promise((resolve, reject) => {
          request.post(verifyUri, {
            form: {
              path: opts.path,
              method: opts.method,
              token: opts.token
            },
            headers: {
              'Content-Type': 'application/json',
              'type': 'jwt',
              'api_key': apiKey,
              'api_secret': apiSecret,
            }
          }, function(err, res, body) {
            if (res && res.statusCode == 200) {
              let session = JSON.parse(body);
              if (session.error) {
                return reject(new Error(session.error || 'Invalid JWT token'));
              } else {
                return resolve(session);
              }
            } else {
              return reject(new Error('Auth-service connection problem'));
            }
          });
        });
        return promise;
      };

      // fetch user profile
      const fetchProfile = function(token) {
        let promise = new Promise((resolve, reject) => {
          request.get(profileUri + '?token=' + token,
            function(err, response, body) {
              if (response.statusCode == 200)
                return resolve(JSON.parse(body).profile);
              else
                return reject(new Error('Error fetching user profile'));
            });
        });
        return promise;
      };

      // creates a new access token from session
      const newAccessToken = function(session){
        debug('Creating new access token on cache');
        let iat = new Date(0).setUTCSeconds(session.iat);
        var accessToken = new AccessToken(Object.assign(session, {
          userId: session.usr,
          ttl: session.exp - session.iat,
          scopes: null, //overwrite the token scopes
          created: new Date(iat).toISOString(),
        }));
        // if the JWT has a defined scope, then attach it to the accessToken
        if(session.scopes && session.scopes[0] != '*:*')
          accessToken.scopes = session.scopes;

        return accessToken.save();
      };

      const encodedJWT = (req.body ? req.body.access_token : null) ||
        (req.headers.authorization ?
          req.headers.authorization.split(' ')[1] : null) ||
        req.query.access_token;

      // If no token on query or body, then skip to the next middleware on the chain
      if (!encodedJWT)
        return next();

      debug('Access Token found on query.');
      const decodedJWT = jwt.decode(encodedJWT);
      var session;

      // If the JWT  it's malformed throw an UNAUTHORIZED error
      if(!decodedJWT)
        return next({
          status: 401,
          code: 'UNAUTHORIZED',
          message: 'Malformed JWT token',
        });

      // Search for the token on the cache/DB
      AccessToken.findOne({where: {
        jti: decodedJWT.jti
      }}).then((token) => {
        // if not token found on cache, then we verify the JWT
        // against the auth-service
        if(!token) {
          debug('Token not found on cache, verifying token...');
          verifyToken({
            path: req.path,
            method: req.method,
            token: encodedJWT,
          }).then((s) => {
            debug('JWT Token verified');
            session = s;
            return User.findById(session.usr);
          }).then((user) => {
            // If the user does not exist, create a new one
            if(!user) {
              debug('Creating new user');
              // Fetch the user profile from the auth-service
              fetchProfile(encodedJWT).then((profile) => {
                let user = new User({
                  id: profile.userId,
                  email: profile.account || profile.email,
                  password: crypto.randomBytes(20).toString('hex'),
                  username: profile.username,
                  emailVerified: true,
                });

                // Save the new user
                user.save().then((user) => {
                  // create the new access token
                  newAccessToken(session).then((accessToken) => {
                    req.user = user;
                    req.accessToken = accessToken;
                    req.jwt = attachJWT ? encodedJWT : null;
                    return next();
                  }).catch((error) => {
                    debug('Error creating token, throwing UNAUTHORIZED error');
                    return next({
                      status: 401,
                      code: 'UNAUTHORIZED',
                      message: error.message || 'Error creating token',
                    });
                  });
                }).catch((error) => {
                  debug('Error creating user, throwing UNAUTHORIZED error');
                  return next({
                    status: 401,
                    code: 'UNAUTHORIZED',
                    message: error.message || 'Error creating user',
                  });
                });
              }).catch((error) => {
                debug('Cant fetch user profile, throwing UNAUTHORIZED error');
                return next({
                  status: 401,
                  code: 'UNAUTHORIZED',
                  message: error.message || 'Invalid JWT profile',
                });
              });
            } else {
              debug('User found, creating access token');
              // create the new access token
              newAccessToken(session).then((accessToken) => {
                req.user = user;
                req.accessToken = accessToken;
                req.jwt = attachJWT ? encodedJWT : null;
                return next();
              }).catch((error) => {
                debug('Error creating token, throwing UNAUTHORIZED error');
                return next({
                  status: 401,
                  code: 'UNAUTHORIZED',
                  message: error.message || 'Error creating token',
                });
              });
            }
          }).catch((error) => {
            debug('Invalid JWT, throwing UNAUTHORIZED error');
            return next({
              status: 401,
              code: 'UNAUTHORIZED',
              message: error.message,
            });
          });
        } else {
          token.validate((error, isValid) => {
            if(!isValid) {
              return next({
                status: 401,
                code: 'UNAUTHORIZED',
                message: 'Expired JWT token',
              });
            } else {
              debug('Valid token found on cache, loading user');
              User.findById(decodedJWT.usr).then((user) => {
                req.user = user;
                req.accessToken = token;
                req.jwt = attachJWT ? encodedJWT : null;
                return next();
              }).catch((error) => {
                return next({
                  status: 401,
                  code: 'UNAUTHORIZED',
                  message: error.message || 'Invalid JWT user',
                });
              });
            }
          })
        }
      }).catch((error) => {
        return next({
          status: 401,
          code: 'UNAUTHORIZED',
          message: error.message,
        });
      });
    };
  }
};
