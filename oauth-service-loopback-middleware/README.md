# oauth-service-loopback-middleware
This middleware provide a easy way to authenticate request with the [auth-service](https://github.com/zekylaf/auth-service) tokens.
As its name implies, is designed for the loopback framework and relays on the user and acess-token built in models.

## How to use it
Create a middleware file on the `server/middleware/`
folder and add to it the code shown bellow. You can name it as you please, for this example will be naming it `auth-request.js`.

```
var OauthServiceMiddleware = require('oauth-service-loopback-middleware');

module.exports = function() {
  const oauthService = new OauthServiceMiddleware('https://auth-service-uri.com');
  return oauthService.authenticate();
};
```

Then declare it on the `middleware.json` file. On the `parse:after` instance add the path to the middleware file and the path where it will be executed.

```
"parse:after":{
  "./middleware/auth-request": {
    "paths": ["/api"]
  }
}
```

Done! If all works fine, now you should see `user` and an `accessToken` objects attached to every authenticated requests.
Also you can allow or deny access to the models methods using the ACL `$authenticated` role.

```
"acls": [{
    "accessType": "*",
    "principalType": "ROLE",
    "principalId": "$everyone",
    "permission": "DENY",
    "property": "*"
  },{
    "accessType": "*",
    "principalType": "ROLE",
    "principalId": "$authenticated",
    "permission": "ALLOW",
    "property": "*"
}]
```
