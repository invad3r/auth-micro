'use strict';
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const debug = require('debug')('views:public');

module.exports = function authRoutes(app) {
  // destroySession: '/oauth/session/destroy?redirectURL=' + (req.authClient ? req.authClient.failureURL : '/account/signin'),
  const ctx = {
    authLocal: '/auth/local',
    oauthImplicit: '/oauth/grant/implicit',
    oauthMFA: '/oauth/grant/two-factor',
    oauthDestroy: '/oauth/token/destroy',
    pubTermsAndConditions: 'https://your-uri-here',
    pubRegister: '/public/register',
    pubSignupForm: '/public/signup',
    pubSigninForm: '/public/signin',
    pubAccountCreated: '/public/created',
    pubAccountVerify: '/public/verify',
    pubAccountVerifyEmail: '/public/verify/email',
    userPassReset: '/public/password-reset',
    userPassChange: '/account/password-change',
    userPassResetEmail: '/public/password-reset/email',
    userAcknowldege: '/account/acknowledge-agreement',
    userFetchApps: '/account/app',
    userFetchSessions: '/account/session',
    userFetchProfile: '/account/profile',
    userFetchPin: '/account/user-pin',
    mfaFetch: '/account/two-factor',
    mfaQRFetch: '/account/two-factor/totp/qr',
    mfaCodeFetch: '/account/two-factor/totp/code',
    mfaDisable: '/account/two-factor/disable',
    mfaForm: '/public/two-factor/config',
    mfaSMSForm: '/public/two-factor/sms',
    mfaSMSEnable: '/account/two-factor/sms/enable',
    mfaSMScode: '/account/two-factor/sms/send-code',
    mfaTOTPForm: '/public/two-factor/totp',
    mfaTOTPEnable: '/account/two-factor/totp/enable',
    clientFetch: '/account/client/:id',
    clientAuthorize: '/account/client/:id/authorize',
    apiKey: app.get('api_key'),
    social: _.chain(app.get('auth-providers'))
      .filter((p) => { return p.provider != 'local'; })
      .map((p) => { return {name: p.provider, path: p.authPath}; })
      .value(),
  };

  const renderTranslatedView = function(req, res, next) {
    const lang = (req.query.lang || req.session.language || 'ES').toLowerCase();
    const view = req.view;
    if (req.session) req.session.language = lang;
    const session = {
      clientId: req.session ? req.session.clientId : null,
      authorized: req.session && req.session.passport ? true : false,
    };
    const context = Object.assign(ctx, {session});
    debug(`rendering ${lang}/${view} translated view`);
    let viewPath = path.join(__dirname, `../../views/${lang}/${view}.ejs`);
    if (fs.existsSync(viewPath)) {
      res.render(`${lang}/${view}`, context);
    } else {
      res.render('en/translation/missing', context);
    }
  };

  /**
  * Renders the login form.
  */
  app.get('/public/signin', function(req, res, next) {
    req.view = 'account/signin';
    return next();
  }, renderTranslatedView);

  /**
  * Renders the sign up form.
  */
  app.get('/public/signup', function(req, res, next) {
    req.view = 'account/signup';
    return next();
  }, renderTranslatedView);

  app.get('/public/created', function(req, res, next) {
    req.view = 'account/created';
    return next();
  }, renderTranslatedView);

  app.get('/public/verified', function(req, res, next) {
    req.view = 'account/verified';
    return next();
  }, renderTranslatedView);

  app.get('/public/password-change/form', function(req, res, next) {
    req.view = 'account/password-change';
    return next();
  }, renderTranslatedView);

  // show password reset form
  app.get('/public/password-reset/form', function(req, res, next) {
    req.view = 'account/password-reset';
    return next();
  }, renderTranslatedView);

  app.get('/public/two-factor/config', function(req, res, next) {
    req.view = 'two-factor/config';
    return next();
  }, renderTranslatedView);

  app.get('/public/two-factor/totp', function(req, res, next) {
    req.view = 'two-factor/enable-totp';
    return next();
  }, renderTranslatedView);

  app.get('/public/two-factor/sms', function(req, res, next) {
    req.view = 'two-factor/enable-sms';
    return next();
  }, renderTranslatedView);

  app.get('/public/two-factor/submit', function(req, res, next) {
    req.view = 'two-factor/submit';
    return next();
  }, renderTranslatedView);

  app.get('/public/me', function(req, res, next) {
    req.view = 'account/me';
    return next();
  }, renderTranslatedView);

  /**
  * Renders the terms and conditions disclaimer.
  */
  app.get('/public/legal', function(req, res, next) {
    req.view = 'legal/terms-and-conditions';
    return next();
  }, renderTranslatedView);

  /**
  * Renders a small welcome disclaimer.
  * this disclaimer will be shown only on the user first login
  */
  app.get('/public/welcome', function(req, res, next) {
    req.view = 'legal/welcome';
    return next();
  }, renderTranslatedView);

  app.get('/public/authorize', function(req, res, next) {
    req.view = 'legal/authorize';
    return next();
  }, renderTranslatedView);
};
