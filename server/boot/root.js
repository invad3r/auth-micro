'use strict';
const publicWeb = require('./web/public');
const publicAPI = require('./api/public');
const oauthAPI = require('./api/oauth');
const accountAPI = require('./api/account');
const twoFactorAPI = require('./api/two-factor');

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  server.use(router);

  // login forms & public site
  publicWeb(server);

  // JSON only endpoints
  publicAPI(server);
  oauthAPI(server);
  accountAPI(server);
  twoFactorAPI(server);
};
