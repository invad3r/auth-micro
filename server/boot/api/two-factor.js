'use strict';
const debug = require('debug')('app:json:two-factor');
const crypto = require('crypto');
const AWS = require('aws-sdk');
const notp = require('notp');
const t2 = require('thirty-two');
const qr = require('qr-image');

module.exports = function authRoutes(app) {
  var TwoFactor = app.models.TwoFactor;
  var TwoFactorToken = app.models.TwoFactorToken;

  /**
  * @summary Return the two-factor configuration for a particular user / client
  * @readonly
  */
  app.get('/account/two-factor', function(req, res, next) {
    const client = req.authClient;
    const user = req.user;

    if (!user || !client)
      res.status(401).send({error: 'UNAUTHORIZED'});

    TwoFactor.findOne({
      where: {and: [
        {clientId: client.id},
        {userId: user.id},
      ]},
    }, function(err, twoFactor) {
      if (err)
        res.status(500).send({error: 'INTERNAL'});
      else
        res.status(200).send({config: twoFactor});
    });
  });

  /**
  * @summary Return a google authenticator complaint QR code for TOTP validation.
  * First look for a preexistent secret on the database,
  * if not found creates a new one and return the QR.
  * @readonly
  */
  app.get('/account/two-factor/totp/code', function(req, res, next) {
    const client = req.authClient;
    const user = req.user;

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    TwoFactor.findOne({
      where: {and: [
        {clientId: client.id},
        {userId: user.id},
      ]},
    }, function(err, twoFactor) {
      let secret;

      if (twoFactor) {
        secret = twoFactor.secret;
      } else {
        // this seed expires every 2.7 hours passed from epoch
        let seed = user.id + client.id + Date.now().toString().substring(1, 7);
        secret = crypto.createHash('sha256').update(seed).digest('hex')
          .substring(0, 10);
      }

      const b32 = t2.encode(secret).toString().replace(/=/g, '');
      return res.status(200).send({code: b32});
    });
  });

  /**
  * @summary Return a google authenticator complaint QR code for TOTP validation.
  * First look for a preexistent secret on the database,
  * if not found creates a new one and return the QR.
  * @readonly
  */
  app.get('/account/two-factor/totp/qr', function(req, res, next) {
    const format = (req.body.format || req.query.format) || 'svg';
    const client = req.authClient;
    const user = req.user;
    const allowedTypes = ['svg', 'uri'];

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    if (!~allowedTypes.indexOf(format))
      return res.status(400).send('Format not supported.');

    TwoFactor.findOne({
      where: {and: [
        {clientId: client.id},
        {userId: user.id},
      ]},
    }, function(err, twoFactor) {
      let secret;

      if (twoFactor) {
        secret = twoFactor.secret;
      } else {
        // this seed expires every 2.7 hours passed from epoch
        let seed = user.id + client.id + Date.now().toString().substring(1, 7);
        secret = crypto.createHash('sha256').update(seed).digest('hex')
          .substring(0, 10);
      }

      const b32 = t2.encode(secret).toString().replace(/=/g, '');
      const uri = `otpauth://totp/${client.name.toLowerCase()}?secret=${b32}`;
      var payload;

      switch (format) {
        case 'uri':
          payload = uri;
          break;

        case 'svg':
          res.set('Content-Type', 'image/svg+xml');
          payload = qr.imageSync(uri, {type: format});
          break;
      }
      return res.status(200).send(payload);
    });
  });

  /**
  * @summary Send a totp code through a sms message
  */
  app.post('/account/two-factor/sms/send-code', function(req, res, next) {
    const client = req.authClient;
    const user = req.user;

    TwoFactor.findOne({
      where: {and: [
        {clientId: client.id},
        {userId: user.id},
      ]},
    }, function(err, twoFactor) {
      let secret, phoneNumber;

      if (twoFactor) {
        phoneNumber = twoFactor.phoneNumber;
        secret = twoFactor.secret;
      } else {
        phoneNumber = req.body.phoneNumber;
        // this seed expires every 2.7 hours passed from epoch
        let seed = user.id + client.id + Date.now().toString().substring(1, 7);
        secret = crypto.createHash('sha256').update(seed).digest('hex');
      }

      const snsConfig = app.get('sns');
      AWS.config.update(snsConfig);

      let sns = new AWS.SNS();

      var params = {
        Message: notp.totp.gen(secret),
        MessageStructure: 'string',
        PhoneNumber: phoneNumber,
      };

      sns.publish(params, function(err, data) {
        if (err) return res.sendStatus(500);
        return res.status(200).send({msg: 'Success'});
      });
    });
  });
};
