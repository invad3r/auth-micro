'use strict';
const debug = require('debug')('oauth:grant');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

module.exports = function authRoutes(app) {
  /**
  * Destroy both access and refresh token
  */
  app.all('/oauth/token/destroy', function(req, res, next) {
    const JwtToken = app.models.JwtToken;
    const AuthClient = app.models.AuthClient;
    const redirect = req.query.redirect;
    const token = req.body.token || req.query.token;

    const decoded = jwt.decode(token);
    if (!decoded) {
      if (redirect)
        return res.redirect(redirect);
      else
        return res.status(400).send({message: 'Malformed JWT'});
    }

    if (req.session)
      req.session.destroy();

    AuthClient.findById(decoded.iss).then((client) => {
      // validate token signature and expiration time
      return JwtToken.resolve(token, client.secret);
    }).then((validToken) => {
      if (!validToken)
        throw new Error('Invalid JWT');
      validToken.destroy();
      if (redirect)
        return res.redirect(redirect);
      else
        return res.status(200).send({message: 'token destroyed'});
    }).catch((error) => {
      return res.status(400).send({message: error.message});
    });
  });

  /**
  * Validate a jwt token
  */
  app.post('/oauth/token/verify', function(req, res, next) {
    const App = app.models.App;
    const JwtToken = app.models.JwtToken;
    const AuthClient = app.models.AuthClient;
    const apiKey = req.headers.api_key || '';
    const apiSecret = req.headers.api_secret || '';
    const token = req.body.token || '';
    const decodedJWT = jwt.decode(token);
    var client, api;

    debug('Verifying jwt scopes and perms');
    App.findOne({where: {and: [
      {key: apiKey},
      {secret: apiSecret},
    ]}}).then((app) => {
      api = app;
      // validate the api key and secret
      if (!api)
        throw new Error('Invalid api key and secret');
      // load the client specified on the jwt iss claim
      if (!decodedJWT)
        throw new Error('Malformed JWT');
      return AuthClient.findById(decodedJWT.iss);
    }).then((authClient) => {
      client = authClient;
      // validate token signature and expiration time
      return JwtToken.resolve(token, client.secret);
    }).then((validToken) => {
      debug('valid JWT token');
      // validate token scopes
      let app = _.find(client.apps, (scope) => {
        return scope.appId.toString() == api.id.toString();
      });

      if (!app)
        throw new Error('Application not in scopes');

      debug('JWT token on app scope');
      decodedJWT.scopes = app.scopes;
      return res.send(decodedJWT);
    }).catch((error) => {
      var status, code;
      if (error.message == 'Application not in scopes') {
        status = 403;
        code = 'FORBIDDEN';
      } else {
        status = 401;
        code = 'UNAUTHORIZED';
      }

      return res.send({
        code: code,
        status: status,
        error: error.message,
      });
    });
  });

  /**
  * Client Credentials Grant (http://tools.ietf.org/html/rfc6749#section-4.4)
  */
  app.post('/oauth/grant/client-credentials', function(req, res, next) {
    const JwtToken = req.app.models.JwtToken;
    const client = req.authClient;
    const session = req.oauth;
    const user = req.headers.usr;

    if (!user)
      return next({
        status: 404,
        code: 'BAD_REQUEST',
        message: 'Missing `usr` header',
      });

    const tokenData = {
      audience: req.audience,
      scopes: req.scopes,
      metadata: req.body.metadata,
      type: req.headers.type,
      userId: req.headers.usr,
      clientId: client.id,
      fingerprint: session.fingerprint,
      origin: session.origin,
      ttl: client.jwtExpTime,
    };

    const jwt = new JwtToken(tokenData);
    return jwt.forge(client.secret).then((signedJWT) => {
      return res.send({
        'token_type': 'jwt',
        'access_token': signedJWT,
      });
    }).catch((error) => {
      return res.status(500).send({message: 'Internal server error'});
    });
  });

  /**
  * Resource Owner Password Credentials Grant (https://tools.ietf.org/html/rfc6749#section-4.3)
  */
  app.post('/oauth/grant/credentials-owner', function(req, res, next) {
    const JwtToken = req.app.models.JwtToken;
    const RefreshToken = req.app.models.RefreshToken;
    const user = req.user;
    const client = req.authClient;
    const session = req.oauth;
    var refreshTokenId;

    const tokenData = {
      userId: user.id,
      clientId: client.id,
      fingerprint: session.fingerprint,
      origin: session.origin,
    };

    new RefreshToken(tokenData).forge().then((token) => {
      if (!token)
        throw new Error('Error creating refresh token');
      refreshTokenId = token.id;
      tokenData.ttl = client.jwtExpTime;
      const jwt = new JwtToken(tokenData);
      return jwt.forge(client.secret);
    }).then((signedJWT) => {
      return res.send({
        'token_type': 'jwt',
        'access_token': signedJWT,
        'refresh_token': refreshTokenId,
      });
    }).catch((error) => {
      return res.status(500).send({message: 'Internal server error'});
    });
  });

  /**
  * Validate the google authenticator code and return a signed JWT
  */
  app.post('/oauth/grant/two-factor', function(req, res, next) {
    debug('executing two-factor token grant');
    const JwtToken = req.app.models.JwtToken;
    const token = req.twoFactorToken;
    const client = req.authClient;

    const jwt = new JwtToken({
      userId: token.userId,
      fingerprint: req.oauth.fingerprint,
      origin: req.oauth.origin,
      clientId: client.id,
      ttl: client.jwtExpTime,
    });
    jwt.forge(client.secret).then((signedJWT) => {
      token.destroy();
      return res.send({
        'token_type': 'jwt',
        'access_token': signedJWT,
      });
    }).catch((error) => {
      return res.status(400).send({error: 'Invalid two-factor token'});
    });
  });

  /**
  * Implicit Grant (http://tools.ietf.org/html/rfc6749#section-4.2)
  */
  app.get('/oauth/grant/implicit', function(req, res, next) {
    const JwtToken = req.app.models.JwtToken;
    const user = req.user;
    const client = req.authClient;
    const session = req.session;

    const jwt = new JwtToken({
      userId: user.id,
      fingerprint: session.fingerprint,
      origin: session.origin,
      clientId: client.id,
      ttl: client.jwtExpTime,
    });
    jwt.forge(client.secret).then((signedJWT) => {
      if (user.firstLogin)
        return res.redirect('/public/welcome?token=' + signedJWT +
          '&clientId=' + client.id);
      else
        return res.redirect(client.callbackURL + '?token=' + signedJWT);
    }).catch((error) => {
      return res.redirect(client.failureURL);
    });
  });

  /**
  * Refresh Token Grant (http://tools.ietf.org/html/rfc6749#section-1.5)
  */
  app.post('/oauth/grant/refresh-token', function(req, res, next) {
    const JwtToken = req.app.models.JwtToken;
    const RefreshToken = req.app.models.RefreshToken;
    const user = req.user;
    const client = req.authClient;
    const session = req.oauth;
    const refreshToken = req.refreshToken;
    var refreshTokenId;

    const tokenData = {
      userId: refreshToken.userId,
      clientId: refreshToken.clientId,
      fingerprint: refreshToken.fingerprint,
      origin: refreshToken.origin,
      ttl: client.jwtExpTime,
    };

    refreshToken.regenerate().then((token) => {
      if (!token)
        throw new Error('Error creating refresh token');
      refreshTokenId = token.id;
      const jwt = new JwtToken(tokenData);
      return jwt.forge(client.secret);
    }).then((signedJWT) => {
      return res.send({
        'token_type': 'jwt',
        'access_token': signedJWT,
        'refresh_token': refreshTokenId,
      });
    }).catch((error) => {
      return res.status(500).send({message: 'Internal server error'});
    });
  });
};
