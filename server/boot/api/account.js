'use strict';

const debug = require('debug')('app:json:account');
const _ = require('lodash');
const path = require('path');
const crypto = require('crypto');
const qs = require('querystring');
const UAParser = require('user-agent-parser');

module.exports = function authRoutes(app) {
  var User = app.models.User;
  var TwoFactor = app.models.TwoFactor;

  app.get('/account/client/:id', function(req, res, next) {
    const AuthClient = req.app.models.AuthClient;
    AuthClient.findById(req.params.id).then((client) => {
      return res.status(200).send({
        name: client.name,
        disclaimer: client.disclaimer,
      });
    }).catch((error) => {
      return res.status(400).send({
        code: 'BAD_REQUEST',
        message: 'Invalid client id',
      });
    });
  });

  /**
  * Creates an authorization register on the database.
  */
  app.post('/account/client/:id/authorize', function(req, res, next) {
    const userAuth = app.models.userAuth;
    userAuth.findOrCreate({
      userId: req.user.id,
      clientId: req.authClient.id,
    }, function(err, userAuth) {
      if (err)
        return res.status(500).send({
          error: 'Unexpected error authorizing client',
        });

      return res.status(200).send({messageg: 'Client authorized'});
    });
  });

  /**
  * @summary Mark the user first login and the 2fa risk acknowledge agreement as accepted
  */
  app.post('/account/acknowledge-agreement', function(req, res) {
    const user = req.user;
    user.no2faRiskAcknowledge = true;
    user.save(function(err) {
      if (err)
        return res.status(err.statusCode).send(err);
      else
        return res.status(200).send({message: 'Success'});
    });
  });

  /**
  * @summary Return the user profile
  * check http://passportjs.org/docs/profile for standarized profile info
  * @readonly
  */
  app.get('/account/user-pin', function(req, res, next) {
    const user = req.user;

    if (!user || !req.user.firstLogin) {
      return res.status(401).send({message: 'UNAUTHORIZED'});
    } else  {
      user.firstLogin = false;
      user.save().then((user) => {
        return res.status(200).send({pin: user.pin});
      }).catch((error) => {
        return res.status(500).send({message: 'INTERNAL'});
      });
    }
  });

  /**
  * @summary Return the user profile
  * check http://passportjs.org/docs/profile for standarized profile info
  * @readonly
  */
  app.get('/account/profile', function(req, res, next) {
    const User = app.models.User;
    const profile = User.parseProfile(req.user);
    return res.status(200).send({profile: profile});
  });

  /**
  * @summary list all the active sessions for the current user
  * @readonly
  */
  app.get('/account/session', function(req, res, next) {
    const JwtToken = app.models.JwtToken;
    const user = req.user;

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    JwtToken.find({where: {
      userId: user.id,
    }}, function(err, tokens) {
      if (err) {
        return res.status(500).send({error: 'INTERNAL'});
      } else {
        var sessions = tokens.map(function(t) {
          let parser = new UAParser();
          parser.setUA(t.origin.agent);
          return {
            token: t.jwt,
            agent: parser.getResult(),
            created: t.created,
          };
        });
        return res.status(200).send(sessions);
      }
    });
  });

  /**
  * @summary list all the client applications associated with the current user
  * @readonly
  */
  app.get('/account/app', function(req, res, next) {
    const user = req.user;
    const UserAuth = app.models.UserAuth;

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    UserAuth.find({where: {
      userId: user.id,
    }, include: {
      relation: 'client',
      scope: {fields: ['name', 'id']},
    }}, function(err, apps) {
      if (err)
        return res.status(500).send({error: 'INTERNAL'});
      else
        return res.status(200).send(apps);
    });
  });

  /**
  * @summary enables the sms 2FA for the current user/client relationship
  * @param {string} totp: the time-based one-time code sent to the user phone
  * @param {string} phoneNumber: the phone number where the future totp codes will be sent
  */
  app.post('/account/two-factor/sms/enable', function(req, res, next) {
    const client = req.authClient;
    const user = req.user;
    const totp = req.body.totp || req.query.totp;
    const phoneNumber = req.body.phoneNumber || req.query.phoneNumber;

    if (!client)
      return res.status(400).send({error: 'MISSING_CLIENT'});

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    if (!phoneNumber)
      return res.status(400).send({error: 'MISSING_PHONE'});

    if (!totp)
      return res.status(400).send({error: 'INVALID_TOTP'});

    let seed = user.id + client.id + Date.now().toString().substring(1, 7);
    const secret = crypto.createHash('sha256').update(seed).digest('hex');

    let twoFactor = new TwoFactor({
      clientId: client.id,
      userId: user.id,
      phoneNumber: phoneNumber,
      secret: secret.substring(0, 10),
      type: 'sms',
    });

    if (!twoFactor.verify(totp, {window: 20}))
      return res.status(400).send({error: 'INVALID_TOTP'});

    twoFactor.save(function(err, twoFactor) {
      if (err)
        return res.status(500).send({error: 'INTERNAL'});
      else
        return res.status(200).send({message: 'Success'});
    });
  });

  /**
  * @summary enables the 2FA with the authenticator app for the current user/client relationship
  * @param {string} totp: the time-based one-time code generated with the authenticator app
  */
  app.post('/account/two-factor/totp/enable', function(req, res, next) {
    const client = req.authClient;
    const user = req.user;
    const totp = req.body.totp || req.query.totp;

    if (!client)
      return res.status(400).send({error: 'MISSING_CLIENT'});

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    if (!totp)
      return res.status(400).send({error: 'INVALID_TOTP'});

    let seed = user.id + client.id + Date.now().toString().substring(1, 7);
    const secret = crypto.createHash('sha256').update(seed).digest('hex');

    let twoFactor = new TwoFactor({
      clientId: client.id,
      userId: user.id,
      secret: secret.substring(0, 10),
      type: 'totp',
    });

    if (!twoFactor.verify(totp))
      return res.status(400).send({error: 'INVALID_TOTP'});

    twoFactor.save(function(err, twoFactor) {
      if (err)
        return res.status(500).send({error: 'INTERNAL'});
      else
        return res.status(200).send({message: 'Success'});
    });
  });

  /**
  * @summary disable the 2FA for the current user/client relationship
  */
  app.post('/account/two-factor/disable', function(req, res, next) {
    const client = req.authClient;
    const user = req.user;

    if (!client)
      return res.status(400).send({error: 'MISSING_CLIENT'});

    if (!user)
      return res.status(401).send({error: 'UNAUTHORIZED'});

    TwoFactor.findOne({where: {and: [
      {clientId: client.id},
      {userId: user.id},
    ]}}, function(err, twoFactor) {
      if (err)
        return res.status(500).send({error: 'INTERNAL'});

      if (!twoFactor)
        return res.status(304).send({message: 'UNCHANGED'});

      twoFactor.destroy();
      return res.status(200).send({message: 'Success'});
    });
  });

  /**
  * @summary update the user's pasword
  * @param {string} oldPassword: user old password
  * @param {string} password: a valid password
  * @param {string} confirmation: the confirmation of the password
  */
  app.post('/account/password-change', function(req, res, next) {
    const oldPassword = req.body.oldPassword;
    const password = req.body.password;
    const confirmation = req.body.confirmation;
    const user = req.user;

    user.hasPassword(oldPassword).then((isMatch) => {
      if (!password || !confirmation || password !== confirmation || !isMatch)
        throw new Error('BAD_PASSWORD');
      return user.updateAttribute('password', req.body.password);
    }).then((user) => {
      debug('>> password reset processed successfully');
      return res.status(200).send({message: 'Success'});
    }).catch(function(error) {
      return res.status(500).send(error.message);
    });
  });
};
