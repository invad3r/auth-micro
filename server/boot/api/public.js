
'use strict';
const debug = require('debug')('app:json:account');
const _ = require('lodash');
const path = require('path');
const crypto = require('crypto');
const qs = require('querystring');
const UAParser = require('user-agent-parser');

module.exports = function authRoutes(app) {
  var User = app.models.User;
  var TwoFactor = app.models.TwoFactor;

  /**
  * @summary Mark the user email as verified
  * @param {string} uid: the id for the user that is going to be updated
  * @param {string} token: the verification token sent to the user email
  */
  app.post('/public/verify', function(req, res, next) {
    const userId = req.body.uid;
    const verificationToken = req.body.token;

    if (!verificationToken)
      return res.status(400).send({
        message: 'Missing token parameter',
        code: 'BAD_REQUEST',
      });

    if (!userId)
      return res.status(400).send({
        message: 'Missing uid parameter',
        code: 'BAD_REQUEST',
      });

    User.confirm(userId, verificationToken, null, function(err) {
      if (err)
        return res.status(400).send({
          code: 'BAD_REQUEST',
          message: 'Invalid token and/or uid parameters',
        });
      else
        return res.status(200).send({message: 'success'});
    });
  });

  /**
  * @summary resend user verification email
  * @param {string} email: the email address where the verify link will be sent
  */
  app.post('/public/verify/email', function(req, res, next) {
    const email = req.body.email || null;
    const app = req.app;

    User.findOne({where: {
      email: email,
    }}, function(err, user) {
      if (err)
        return res.status(500).send({error: 'INTERNAL'});
      if (!user)
        return res.status(404).send({message: 'User not found'});
      const tmplPath = path.resolve(__dirname,
        `../../views/${user.language}/email-template`);
      const options = _.extend(app.get('email'), {
        type: 'email',
        to: user.email,
        subject: app.loopback.template(`${tmplPath}/verify_subject.ejs`)(),
        template: `${tmplPath}/verify.ejs`,
        verifyHref: app.get('verifyPath') +
        '?' + qs.stringify({
          uid: '' + user.id,
          clientId: '' + (req.header.client_id || user.referer),
        }),
        user: user,
      });

      user.verify(options, function(err, response) {
        if (err)
          return res.status(500).send({error: 'INTERNAL'});
        else
          return res.status(200).send({message: 'Success'});
      });
    });
  });

  /**
  * @summary creates the new user and send a verify link to his email
  * @param {string} email: the user email address where the verify link will be sent
  * @param {string} password: the user password
  * @param {boolean} licenseAgreement: if the user accepts or not the license agreement
  */
  app.post('/public/register', function(req, res, next) {
    if (!req.body.licenseAgreement)
      return res.status(403).send({
        code: 'BAD_REQUEST',
        message: 'The terms & conditions must be accepted in order to register',
      });

    var user = new User({
      email: req.body.email,
      password: req.body.password,
      licenseAgreement: true,
      referer: req.headers.client_id,
      language: req.session ? req.session.language : app.get('defaultLanguage'),
    });

    user.isValid(function(valid) {
      if (!valid) {
        if (user.errors.email &&
          user.errors.email.includes('Email already exists'))
          return res.status(400).send({error: 'DUPLICATED_USER'});
        else
          return res.status(400).send({error: 'BAD_REQUEST'});
      }

      user.save(function(err) {
        if (err)
          return res.status(500).send({error: 'INTERNAL'});
        else
          return res.status(200).send(user);
      });
    });
  });

  /**
  * @summary reset the user's pasword
  * @param {string} uid: the id for the user that is going to be updated
  * @param {string} token: the verification token sent to the user email
  * @param {string} password: a valid password
  * @param {string} confirmation: the confirmation of the password
  */
  app.post('/public/password-reset', function(req, res, next) {
    const AccessToken = app.models.AccessToken;
    const verificationToken = req.body.token;
    // const oldPassword = req.body.oldPassword;
    const password = req.body.password;
    const confirmation = req.body.confirmation;
    var currentUser, userId;

    if (!verificationToken)
      return res.status(400).send({error: 'INVALID_TOKEN'});

    if (!password || !confirmation || password !== confirmation)
      return res.status(400).send({message: 'BAD_PASSWORD'});
    AccessToken.findById(verificationToken).then(function(token) {
      if (!token) throw new Error('INVALID_TOKEN');
      userId = token.userId;
      return token.isValid();
    }).then(function(validToken) {
      if (!validToken) throw new Error('EXPIRED_TOKEN');
      return User.findById(userId);
    }).then(function(u) {
      if (!u) throw new Error('INVALID_USER');
      currentUser = u;
      //  return currentUser.hasPassword(oldPassword);
      // }).then(function(isMatch) {
      //  if (!isMatch) throw new Error('BAD_PASSWORD');
      return currentUser.updateAttribute('password', req.body.password);
    }).then(function() {
      debug('>> password reset processed successfully');
      return res.status(200).send({message: 'Success'});
    }).catch(function(error) {
      return res.status(500).send('INTERNAL');
    });
  });

  /**
  * @summary send an email with instructions to reset an existing user password
  * @param {string} email: the user email address where the reset link will be sent
  */
  app.post('/public/password-reset/email', function(req, res, next) {
    User.resetPassword({
      email: req.body.email,
    }, function(err) {
      if (err)
        return res.status(err.statusCode).send(err.code);
      else
        return res.status(200).send({message: 'Success'});
    });
  });
};
