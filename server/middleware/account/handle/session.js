'use strict';

const debug = require('debug')('account:middleware:handle:session');
const _ = require('lodash');
/**
* Authorize a user using session cookies
* This middleware will check if the requested resource is whithin the token scope.
*/
module.exports = function() {
  return function(req, res, next) {
    debug('handling session on /account request');
    const Application = req.app.models.App;
    const AuthClient = req.app.models.AuthClient;
    const apiKey = req.app.get('api_key');
    // const apiKey = req.headers.api_key || req.query.api_key;
    const clientId = req.session ? req.session.clientId : null;

    if (!apiKey || !clientId) {
      debug('No session found, skipping authorization process');
      return next();
    }

    Promise.all([
      AuthClient.findById(clientId),
      Application.findOne({where: {key: apiKey}}),
    ]).then((data) => {
      const client = data[0];
      const application = data[1];

      if (!client || !application)
        return next();

      // Load the client into the request object
      req.authClient = client;
      req.application = application;

      return next();
    }).catch((error) => {
      return next({status: 500, code: 'INTERNAL', message: error.message});
    });
  };
};
