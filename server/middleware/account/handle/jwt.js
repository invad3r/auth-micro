'use strict';
const jwt = require('jsonwebtoken');
const debug = require('debug')('account:middleware:handle:jwt');
/**
* Authenticate a user with a JWT bearer scheme
* This middleware parse and verify the JWT token from the authorization header.
* Once the JWT token it's verified the middleware will load the user and client
* data on the request object.
*/
module.exports = function() {
  return function(req, res, next) {
    debug('handling JWT on /account request');
    const JwtToken = req.app.models.JwtToken;
    const User = req.app.models.User;
    const Application = req.app.models.App;
    const AuthClient = req.app.models.AuthClient;
    const apiKey = req.app.get('api_key');
    // const apiKey = req.headers.api_key || req.query.api_key;
    const token = req.headers.authorization ?
      req.headers.authorization.split(' ')[1] : req.query.token;
    const decoded = jwt.decode(token);
    const clientId = req.headers.client_id || req.query.clientId ||
      (decoded ? decoded.iss : null);

    if (token && !decoded)
      debug('Malformed JWT');

    if (!clientId || !token) {
      debug('No JWT found, skipping authorization process');
      return next();
    }

    Promise.all([
      AuthClient.findById(clientId),
      Application.findOne({where: {key: apiKey}}),
    ]).then((data) => {
      const client = data[0];
      const application = data[1];

      if (!client || !application)
        throw new Error('Invalid client or application id');

      // Load the client into the request object
      req.authClient = client;
      req.application = application;

      return JwtToken.resolve(token, client.secret);
    }).then((JWToken) => {
      // Load the accessToken into the request object
      req.accessToken = JWToken;
      return User.deserializeById(JWToken.userId);
    }).then((user) => {
      // Load the user into the request object
      req.user = user;
      return next();
    }).catch((error) => {
      debug('Error resolving jwt');
      debug(error);
      return next();
      // return next({status: 500, code: 'INTERNAL', message: error.message});
    });
  };
};
