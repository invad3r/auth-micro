'use strict';

const debug = require('debug')('account:middleware:authorization');
const _ = require('lodash');
/**
* Authorize a user with a JWT bearer scheme
* This middleware will check if the requested resource is whithin the token scope.
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing /account request authorization');
    const path = req.path.toLowerCase();
    const method = req.method.toLowerCase();
    const application = req.application;
    const client = req.authClient;

    if (!application)
      return next({
        status: 403,
        code: 'FORBIDDEN',
        message: 'Missing or invalid application id',
      });

    if (!client)
      return next({
        status: 403,
        code: 'FORBIDDEN',
        message: 'Missing or invalid client id',
      });

    // Check if the request is in the jwt scopes
    let app = _.find(client.apps, (app) => {
      return app.appId.toString() == application.id.toString();
    });

    if (!app)
      return next({
        status: 403,
        code: 'FORBIDDEN',
        message: 'Request out of token scope',
      });

    // for now we leave these commented out, but in the future
    // we may consider to have a more fine grained ACL's using the
    // application scopes
    // req.accessToken.scopes = app.scopes;
    return next();
  };
};
