'use strict';

const debug = require('debug')('account:middleware:authentication');
/**
* Authenticate a user with a JWT bearer scheme
* This middleware parse and verify the JWT token from the authorization header.
* Once the JWT token it's verified the middleware will load the user and client
* data on the request object.
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing /account request authentication');

    if (!req.user)
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: 'Invalid authentication data',
      });
    else
      return next();
  };
};
