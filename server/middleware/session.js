'use strict';

var debug  = require('debug')('oauth:middleware:session');
var crypto = require('crypto');

module.exports = function() {
  return function(req, res, next) {
    const session = req.session ? req.session : {};
    const clientId = req.headers.client_id ||
      req.query.client_id || req.query.clientId || session.clientId;

    debug('executing session middleware on %s', req.url);
    // debug('Client id %s', clientId);

    var origin = {
      ip: req.socket.remoteAddress,
      origin: req.get('origin'),
      referer: req.get('referer'),
      host: req.get('host'),
      agent: req.get('user-agent'),
    };

    const fingerprint = crypto.createHash('md5')
      .update([req.socket.remoteAddress,
        clientId,
        req.get('host'),
        req.get('user-agent'),
      ].join())
      .digest('hex');

    if (req.session) {
      req.session.clientId    = clientId;
      req.session.fingerprint = fingerprint;
      req.session.origin      = origin;
    }

    req.oauth = {
      clientId: clientId,
      fingerprint: fingerprint,
      origin: origin,
    };

    next();
  };
};
