'use strict';

// eslint-disable-next-line max-len
const debug = require('debug')('oauth:grant:two-factor:middleware:authentication');

/**
* Authenticate a two-factor token grant type
* At this point the request object should have the authorization-client loaded into it.
* - Check for the two-factor token on the request headers
* - Validate the ttl of the two-factor token
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing two-factor token grant authentication');
    const UserAuth = req.app.models.UserAuth;
    const TwoFactor = req.app.models.TwoFactor;
    const TwoFactorToken = req.app.models.TwoFactorToken;
    const client = req.authClient;
    const tokenId = req.headers.authorization.split(' ')[1];
    const twoFactorCode = req.headers.code;

    TwoFactorToken.findById(tokenId).then((token) => {
      if (!token)
        throw new Error('Invalid or missing token');

      // Validate the two-factor token
      return new Promise((resolve, reject) => {
        token.validate((error, isValid) => {
          if (error || !isValid)
            return reject(new Error('The token has expired'));
          else
            return resolve(token);
        });
      });
    }).then((token) => {
      // validate that the client id set on the token is the same that the one sent on the query string
      if (String(token.clientId) != String(client.id))
        throw new Error('Invalid client id on query string or header');
      // Search for the two-factor configuration
      req.twoFactorToken = token;
      return TwoFactor.findOne({where: {and: [
        {clientId: token.clientId},
        {userId: token.userId},
      ]}});
    }).then((twoFactor) => {
      // validate the totp code
      if (!twoFactor)
        throw new Error('No user/client two-factor configuration');
      let isValid = twoFactor.verify(twoFactorCode);
      if (!isValid)
        throw new Error('Invalid code, try again');
      else
        return next();
    }).catch((error) => {
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: error.message,
      });
    });
  };
};
