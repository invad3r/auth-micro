'use strict';
const _ = require('lodash');
const debug = require('debug')('oauth:grant:middleware:authorization');
/**
* Authorize a /oauth/grant/* request
* - Check the for client id on the request headers and query
* - Validate the client and load it into the request object
* - Check for the allowed grant types for the current client
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing request client authorization');

    const AuthClient = req.app.models.authClient;
    const clientId = req.headers.client_id || req.query.clientId ||
      (req.session ? req.session.clientId : null);

    if (!clientId)
      return next({
        status: 400,
        code: 'BAD_REQUEST',
        message: 'Missing client id',
      });

    AuthClient.findById(clientId, function(err, client) {
      if (err || !client)
        return next({
          status: 403,
          code: 'FORBIDDEN',
          message: 'Invalid client id',
        });

      // check the scopes
      if (req.headers.scopes) {
        let allScopes = _.reduce(client.apps, (acc, app) => {
          return _.concat(acc, app.scopes);
        }, []);
        let allowedScopes = _.filter(allScopes, (scope) => {
          return req.headers.scopes.split(' ').includes(scope);
        });

        // some of the requested scopes are not allowed
        if (allowedScopes.length < req.headers.scopes.split(' ').length)
          return next({
            status: 403,
            code: 'FORBIDDEN',
            message: 'Exceeded token scope',
          });

        req.scopes = allowedScopes;
      }

      if (req.headers.audience) {
        let audience = _.filter(client.apps, (app) => {
          return req.headers.audience.split(' ').includes(app.name);
        });

        // some of the requested audience are not allowed
        if (audience.length < req.headers.audience.split(' ').length)
          return next({
            status: 403,
            code: 'FORBIDDEN',
            message: 'Exceeded token audience',
          });

        req.audience = audience;
      }

      let path = req.path.replace('/', '');
      if (client.grants.includes(path)) {
        req.authClient = client;
        return next();
      } else {
        return next({
          status: 403,
          code: 'FORBIDDEN',
          message: 'Not authorized grant type',
        });
      }
    });
  };
};
