'use strict';

// eslint-disable-next-line max-len
const debug = require('debug')('oauth:grant:refresh-token:middleware:authentication');

/**
* Authenticate a refresh-token grant type
* At this point the request object should have the authorization-client loaded into it.
* - Validate the ttl of the refresh-token
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing refresh-token grant authentication');
    const RefreshToken = req.app.models.RefreshToken;
    const User = req.app.models.User;
    const UserAuth = req.app.models.UserAuth;
    const clientSecret = req.headers.client_secret;
    const client = req.authClient;
    const refreshTokenId = req.authorization;

    if (!req.headers.authorization)
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: 'No authorization header',
      });

    const scheme = req.headers.authorization.split(' ')[0];
    const tokenId = req.headers.authorization.split(' ')[1];

    if (scheme.toLowerCase() != 'bearer')
      return next({
        status: 400,
        code: 'BAD_REQUEST',
        message: 'Invalid auth strategy',
      });

    // Check the client secret
    if (client.secret != clientSecret)
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: 'Invalid client',
      });

    RefreshToken.findById(tokenId).then((token) => {
      if (!token)
        throw new Error('Invalid refresh token');
      return token.validate();
    }).then((token) => {
      if (!token)
        throw new Error('Expired refresh token');
      req.refreshToken = token;
      return next();
    }).catch((error) => {
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: error.message,
      });
    });
  };
};
