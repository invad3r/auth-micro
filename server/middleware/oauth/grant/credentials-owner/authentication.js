'use strict';

// eslint-disable-next-line max-len
const debug = require('debug')('oauth:grant:credentials:middleware:authentication');
const _ = require('lodash');
/**
* Authenticate a credentials-owner grant type
* At this point the request object should have the authorization-client loaded into it.
* - Check the client secret
* - Check for the user:password credentials on the request header
* - Handle the user/client authorization process
* - Handle the two-factor process, if it's enabled respond with a two-factor-token
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing crentials-owner grant authentication');
    const User = req.app.models.User;
    const UserAuth = req.app.models.UserAuth;
    const TwoFactorToken = req.app.models.TwoFactorToken;
    const TwoFactor = req.app.models.TwoFactor;
    const clientSecret = req.headers.client_secret;
    const client = req.authClient;

    if (!req.get('authorization'))
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: 'Missing basic authorization header',
      });

    const credentials = new Buffer(req.get('authorization').split(' ')
      .pop(), 'base64').toString('ascii').split(':');

    debug('executing credentials-owner grant middleware');

    const username = credentials[0];
    const password = credentials[1];
    var currentUser;

    // Check the client secret
    if (client.secret != clientSecret)
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: 'Invalid client',
      });

    // Check for users credentials
    User.findOne({where: {email: username}}).then((user) => {
      if (!user)
        throw new Error('Invalid user or password');
      currentUser = user;
      return user.hasPassword(password);
    }).then((isMatch) => {
      if (!isMatch)
        throw new Error('Invalid user or password');
      if (User.settings.emailVerificationRequired && !currentUser.emailVerified)
        throw new Error('The user is not verified');
      // if the user and password are valid, then load the user on the request
      req.user = currentUser;

      // check the user and client roles
      if (!_.intersection(currentUser.roles, client.roles).length)
        throw new Error('The user role is not allowed');

      // automatically creates an userAuth register if not already exists
      return UserAuth.findOrCreate({where: {and: [
        {userId: currentUser.id},
        {clientId: client.id},
      ]}}, {
        userId: currentUser.id,
        clientId: client.id,
      });
    }).then((userAuth) => {
      // check for two-factor configuration
      return TwoFactor.findOne({where: {and: [
        {userId: currentUser.id},
        {clientId: client.id},
      ]}});
    }).then((twoFactor) => {
      if (!twoFactor) {
        return next();
      } else {
        // if the two-factor authentication is enabled then interrupt the
        // credentials-owner grant process and return a two-factor token.
        let twoFactorToken = new TwoFactorToken({
          userId: currentUser.id,
          clientId: client.id,
          type: twoFactor.type,
        });
        return twoFactorToken.forge((err, token) => {
          if (err)
            throw new Error('Error creating two-factor token');
          return res.send({
            'token_type': 'two-factor',
            'expires_in': token.ttl,
            'access_token': token.id,
          });
        });
      }
    }).catch((error) => {
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: error.message,
      });
    });
  };
};
