'use strict';

// eslint-disable-next-line max-len
const debug = require('debug')('oauth:grant:implicit:middleware:authentication');
const _ = require('lodash');

/**
* Authenticate a implict grant type
* At this point the request object should have the authorization-client loaded into it.
* - Check for the user and access-token to be loaded on the request object
* - Handle the client authorization process, if it does not exist redirect the user to the
*   authorization page
* - Handle the two-factor process, if it's enabled redirect to the 2FA submit page with
*   a two-factor-token set on the query string
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing implict grant authentication');
    const UserAuth = req.app.models.UserAuth;
    const TwoFactorToken = req.app.models.TwoFactorToken;
    const TwoFactor = req.app.models.TwoFactor;
    const user = req.user; // this comes from the session
    const client = req.authClient; // and this from the authorization middleware

    /**
    * If there is no user object attached to the request
    * rederict the user to the client failureURL property.
    */
    if (!user)
      return res.redirect(client.failureURL);

    // check the user and client roles
    if (!_.intersection(user.roles, client.roles).length)
      return res.redirect(client.failureURL);

    const query = {where: {and: [
      {userId: user.id},
      {clientId: client.id},
    ]}};

    Promise.all([
      TwoFactor.findOne(query),
      UserAuth.findOne(query),
    ]).then((auth) => {
      const twoFactor = auth[0];
      const userAuth = auth[1];
      /**
      * If the user/client relation does not exist and the client
      * skipAuthorization option is set to true, then create the user/client
      * relation and continue to the next step on the middleware chain.
      */
      if (!userAuth && client.skipAuthorization) {
        debug('auto authorize to client application');
        new UserAuth({
          userId: user.id,
          clientId: client.id,
        }).save((error) => {
          if (error)
            throw new Error('Cant authorize client');
          return next();
        });
      } else if (!userAuth) {
        /**
        * If the user/client relation does not exist,
        * then redirect the user to the authorize client page
        */
        debug('rederict to client authorization page');
        return res.redirect('/public/authorize');
      } else if (userAuth && twoFactor) {
        /**
        * If the user/client relation exist and it has the two-factor
        * authentication enabled
        */
        debug('rederict to the submit two-factor page');
        let twoFactorToken = new TwoFactorToken({
          userId: user.id,
          clientId: client.id,
          type: twoFactor.type,
        }).save((err, token) => {
          if (err)
            throw new Error('Cant create two-factor token');
          return res.redirect('/public/two-factor/submit?' +
            'twoFactorToken=' + token.id + '&grant=' + twoFactor.type +
             '&clientId=' + client.id + '&redirect=' + client.callbackURL
           );
        });
      } else {
        debug('authorized request, continue to implict grant');
        /**
        * If none of the previous conditions where meet, then
        * continue to the next step on the middleware chain.
        */
        return next();
      }
    }).catch((error) => {
      debug('Implicit authentication error %s', error.message);
      return res.redirect(client.failureURL);
    });
  };
};
