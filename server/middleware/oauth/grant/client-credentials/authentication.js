'use strict';
const debug = require('debug')('oauth:grant:client:middleware:authentication');

/**
* Authenticate a client-credentials grant type
* At this point the request object should have the authorization-client loaded into it.
* - Check the client secret
*/
module.exports = function() {
  return function(req, res, next) {
    debug('executing client-crentials grant authentication');
    const App = req.app.models.app;
    const apiKey = req.headers.api_key;
    const apiSecret = req.headers.api_secret;

    App.findOne({where: {and: [
      {key: apiKey},
      {secret: apiSecret},
    ]}}).then((app) => {
      if (!app)
        throw new Error('Invalid api key and secret');
      return next();
    }).catch((error) => {
      return next({
        status: 401,
        code: 'UNAUTHORIZED',
        message: 'Invalid api key and secret',
      });
    });
  };
};
