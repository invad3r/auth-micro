'use strict';
require('events').defaultMaxListeners = 100;
const _ = require('lodash');
const crypto = require('crypto');
const app = require('../server');

console.log('Checking database initialization');
const Application = app.models.app;
const AuthClient = app.models.authClient;
const KEYS = [];

var databaseConfig;
try {
  databaseConfig = require('./db.json');
} catch (error) {
  console.log('No initialization file');
  return false;
}

const truncateDB = function(error) {
  console.log('Truncanted database');
  console.log(error);
  AuthClient.destroyAll().then(() => {
    return AuthClient.destroyAll();
  }).then(() => {
    process.exit();
  }).catch((error) => {
    process.exit();
  });
};

const doneSavingApps = _.after(databaseConfig.apps.length, () => {
  createClients(databaseConfig.clients);
});

const doneSavingClients = _.after(databaseConfig.clients.length, (err) => {
  console.log('Database initialization complete');
  console.log(' TAKE NOTE OF THE KEYS & SECRETS BELLOW ');
  console.log(' ');

  let keys = KEYS.sort((a, b) => {
    let nameA = a.split('_')[2];
    let nameB = b.split('_')[2];
    if (nameA < nameB) return -1;
    if (nameA > nameB) return 1;
  });
  keys.forEach((k) => { console.log(k); });
  process.exit();
});

const generateNewAPIKey = function(name, type) {
  let seed = name + Date.now().toString() +
    crypto.randomBytes(8).toString('hex');
  const key = crypto.createHash('sha256').update(seed).digest('hex');
  let k = `AUTH_SERVICE_${name.replace(/-/g, '_').toUpperCase()}`;
  k += `_${type.toUpperCase()}_KEY=${key}`;
  KEYS.push(k);
  return key;
};

const generateNewAPISecret = function(name, type) {
  let seed = name + Date.now().toString() +
    crypto.randomBytes(8).toString('hex');
  const secret = crypto.createHash('sha256').update(seed).digest('hex');
  let k = `AUTH_SERVICE_${name.replace(/-/g, '_').toUpperCase()}`;
  k += `_${type.toUpperCase()}_SECRET=${secret}`;
  KEYS.push(k);
  return secret;
};

// create apps
const createApps = (apps) => {
  _.each(apps, (data) => {
    const app = {
      name: data.name,
      type: data.type,
      key: generateNewAPIKey(data.name, 'api'),
      secret: generateNewAPISecret(data.name, 'api'),
    };

    Application.upsertWithWhere({
      name: app.name,
    }, app, (error, app) => {
      if (error) {
        truncateDB(error);
      } else {
        doneSavingApps();
      }
    });
  });
};

// create apps
const createClients = (clients) => {
  Application.find().then((clientApps) => {
    _.each(clients, (data) => {
      const apps = _.chain(clientApps)
        .filter((a) => { return data.apps.includes(a.name); })
        .map((a) => {
          return {
            appId: a.id,
            name: a.name,
            scopes: ['*:*'],
          };
        }).value();

      const client = {
        name: data.name,
        key: generateNewAPIKey(data.name, 'client'),
        secret: generateNewAPISecret(data.name, 'client'),
        callbackURL: data.callbackURL,
        skipAuthorization: data.skipAuthorization,
        jwtExpTime: data.jwtExpTime,
        grants: data.grants,
        apps: apps,
        disclaimer: [],
        roles: data.roles,
      };
      AuthClient.upsertWithWhere({
        name: client.name,
      }, client, (error, client) => {
        if (error) {
          truncateDB(error);
        } else {
          let k = 'AUTH_SERVICE_';
          k += client.name.replace(/-/g, '_').toUpperCase();
          k += `_CLIENT_ID=${client.id}`;
          KEYS.push(k);
          doneSavingClients();
        }
      });
    });
  }).catch((error) => {
    truncateDB(error);
  });
};

createApps(databaseConfig.apps);
