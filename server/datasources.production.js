'use strict';

module.exports = {
  'db': {
    'host': 'localhost',
    'port': 27017,
    'database': 'auth-service',
    'password': process.env.AUTH_DB_MONGO_PASSWORD,
    'name': 'db',
    'user': process.env.AUTH_DB_MONGO_USER,
    'url': process.env.AUTH_DB_MONGO_URI,
    'connector': 'loopback-connector-mongodb',
    'debug': true,
  },
  'emailDatasoruce': {
    'name': 'emailDatasoruce',
    'connector': 'mail',
    'transports': [{
      'type': 'SES',
      'accessKeyId': process.env.AWS_SES_KEY,
      'secretAccessKey': process.env.AWS_SES_SECRET,
      'rateLimit': 5,
    }],
  },
};
