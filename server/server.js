'use strict';
// var debug = require('debug')('loopback:server');
var loopback = require('loopback');
var boot = require('loopback-boot');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var _ = require('lodash');

var app = module.exports = loopback();

// Passport configurators..
var loopbackPassport = require('loopback-component-passport');
var PassportConfigurator = loopbackPassport.PassportConfigurator;
var passportConfigurator = new PassportConfigurator(app);

/**
* Flash messages for passport
*
* Setting the failureFlash option to true instructs Passport to flash an
* error message using the message given by the strategy's verify callback,
* if any. This is often the best approach, because the verify callback
* can make the most accurate determination of why authentication failed.
*/
var flash = require('express-flash');

// Setup the view engine (jade)
var path = require('path');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use('/static', loopback.static(path.resolve(__dirname, '../static')));

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // to support JSON-encoded bodies
  app.middleware('parse', bodyParser.json({limit: '50mb'}));
  // to support URL-encoded bodies
  app.middleware('parse', bodyParser.urlencoded({
    extended: true,
  }));

  // The access token is only available after boot
  app.middleware('auth', loopback.token({
    model: app.models.accessToken,
  }));

  app.middleware('session:before', cookieParser(app.get('cookieSecret')));
  app.middleware('session', [
    '/public/',
    '/account/client/',
    '/account/two-factor/',
    '/account/acknowledge-agreement',
    '/oauth/grant/implicit',
    '/oauth/token/destroy',
    '/auth',
  ], session(_.extend(
    app.get('session'),
    {store: new MongoStore({
      url: app.datasources.db.settings.url,
      autoRemove: true,
    })}
  )));

  passportConfigurator.init();

  // We need flash messages to see passport errors
  app.use(flash());

  if (process.argv[2] != 'no-providers') {
    passportConfigurator.setupModels({
      userModel: app.models.user,
      userIdentityModel: app.models.userIdentity,
      userCredentialModel: app.models.userCredential,
    });
    var authProviders = app.get('auth-providers');
    for (var s in authProviders) {
      var c = authProviders[s];
      c.session = c.session !== false;
      passportConfigurator.configureProvider(s, c);
    }
  }
  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
