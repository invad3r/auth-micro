# Auth-Service
 The `auth-service` provides a way to grant fine grained access to multiple actors through all the
applications, clients and users ecosystem.
The words application, client, user and grant are not randomly picked and have particular meaning for this implementation.
Here are some basic concepts on what each of them stand for.

#### Applications
  An Application represent every part of a system that requires authentication to exchange information, usually an API.
Lets say that our systems it's an e-commerce and it's divided, let's say in three sub systems,
Payments, Shipping and Inventory API. Each of them has to communicate with the client and to each other.

#### Clients
  A Client represents any Application consumer, e.g. an SPA, mobile app or a simple API client.
Each of this clients may require access to one or more Applications on your system.

#### Grants
  There are many ways on which an user can probe that he is who he claims and therefore gain access to protected
resources. The most commonly known it's providing username and password, this grant is known as credentials owner.

#### User
  A user can act over the system through multiple Clients. Each of this Clients can access a particular set
of Applications when the user probes his identity.
A registered user can have or not access to different Clients, e.g. a regular user on our
e-commerce system will have access to the SPA and mobile app, but a user with the "developer" role can also have access to the API client.


## Common Scenarios
Here are some common scenarios where the `auth-service` can be implemented.

#### SPA & API scenario:
 Probably the most common implementation on the web, a bundled HTML, CSS and Javascript application with register and login forms and a API that serves the data required for each user of the application.

#### SaaS Software and Complex systems
 Not to different from the SPA & API scenario, but with some few more parts, e.g. an SPA for the end users, a Backoffice
application (admin's only), a mobile application and a set of API's.

#### Automation & Microservices
 In order to provide automation or maintain a microservices ecosystem we need to provide a way to grant access to many
different resources to many different actors while keeping our services loosely coupled and within a bounded context. And always keeping this services interactions light and few.

## Grant Types

#### Client Credentials
Suitable for machine-to-machine authentication. An API client would be a good example of this grant.
The API client will use a key and a secret to obtain an JWT token from the authorization server.

#### Credentials Owner
Intended for trusted first party clients both on the web and in native device applications
The client will ask the user for their authorization credentials and then send them to the
authorization server that will respond with a JWT token.

#### Implicit
This grant it's intended to be used for user-agent-based clients (e.g. SPA) that can’t keep a client secret
because all of the application code and storage is easily accessible.
In this case the user will be redirected to the authorization server and asked to login and approve the client.
If the user approves the client they will be redirected from the authorization server back to the client (specifically to the redirect URI) with a JWT token.

#### Refresh Token
Access tokens eventually expire; however some grants respond with a refresh token which enables the client to get a new access token without requiring any other interaction.

#### Two Factor
This grant works with the Implict and the Credentials Owner grants only. Adding an extra challenge to the user in order
to proves his identity with time based one time password (totp) or proof of possession, that once passed
a JWT will be returned by the authorization server.
